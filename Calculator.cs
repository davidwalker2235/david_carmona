﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr_stringcalculator
{
    class Calculator
    {
        Char[] DELIMITER = new Char[] { ',', '\n' };

        public int Add (String text)
        {
            List<char> stringAry = new List<char>();
            if (text.Contains("//"))
            {
                DELIMITER = GetCustomDelimiter(text);
                text = text.Substring(4);
            }

            var numbersAry = GetNumbersAry(text, DELIMITER);
            return GetSum(numbersAry);
        }

        private char[] GetCustomDelimiter (String text)
        {
            var delimiter = DELIMITER;
            List<char> charAry = GetCharsAry(text);
            try
            {
                if (text.Contains("//"))
                {
                    DELIMITER = new char[] { charAry[2] };
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return DELIMITER;
        }

        private List<char> GetCharsAry(String text)
        {
            List<char> stringAry = new List<char>();

            stringAry = text.ToCharArray().ToList();

            return stringAry;
        }

        private string[] GetNumbersAry(String text, char[] delimiter)
        {

            return text.Split(delimiter);
        }

        private int GetSum(string[] numAry)
        {
            int result = 0;
            foreach (var item in numAry)
            {
                int value = Convert.ToInt32(item);
                if (IsNegativeValue(value))
                {
                    throw new Exception("There is a negative value on string");
                }
                result += value;
            }

            return result;
        }

        private bool IsNegativeValue(int value)
        {
            return value < 0;
        }
    }
}
