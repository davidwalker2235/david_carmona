﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr_stringcalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            const string DEFAULT_STRING = "1\n2,3";
            const string STRING_WITH_CUSTOM_DELIMITER = "//;\n1;2";
            const string STRING_WITH_A_NEGATIVE_VALUE = "//;\n1;2;-5";

            Console.Title = "Friendlyrentals string calculator kata";
            Console.WriteLine("Follow instructions on 'readme.md'");

            try
            {
                Calculator calculator = new Calculator();
                Console.WriteLine("Default String: " + calculator.Add(DEFAULT_STRING));
                Console.WriteLine("String with custom delimiter: " + calculator.Add(STRING_WITH_CUSTOM_DELIMITER));
                Console.WriteLine("String with a negative value: " + calculator.Add(STRING_WITH_A_NEGATIVE_VALUE));
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: " + e.Message);
            }

            Console.ReadLine();
        }
    }
}
